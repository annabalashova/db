<?php


namespace Packages\DB;


use Packages\DB\QueryBuilderInterface;
use mysqli;
use \Packages\DB\Db;

class QueryBuilder implements QueryBuilderInterface
{
    public $columns = [];
    public $conditions = [];
    public $table;
    public $limit;
    public $offset;
    public $order = [];
    //public $mysqli;

    public function select($columns): QueryBuilderInterface
    {
        $this->columns = $columns;
        return $this;
    }

    public function where($conditions): QueryBuilderInterface
    {
        $this->conditions = $conditions;
        return $this;
    }

    public function table($table): QueryBuilderInterface
    {
        $this->table = $table;
        return $this;
    }

    public function limit($limit): QueryBuilderInterface
    {
        $this->limit = $limit;
        return $this;
    }

    public function offset($offset): QueryBuilderInterface
    {
        $this->offset = $offset;
        return $this;
    }

    public function order($order): QueryBuilderInterface
    {
        $this->order = $order;
        return $this;
    }

    public function build(): string
    {
        return 'SELECT ' . implode(', ', $this->columns)
            . ' FROM ' . $this->table . ' WHERE ' . implode('', array_keys($this->conditions)) . ' = '
            . "'" . implode('', array_values($this->conditions)) . "'" . ' ORDER BY '
            . implode('', array_keys($this->order)) . ' ' . implode('', array_values($this->order))
            . ' LIMIT ' . $this->limit . ' OFFSET ' . $this->offset ;
    }

    public function one(): ?array
    {
        Db::getInstance()->link();
        $sql = $this->build();
        $result = Db::getInstance()->sendQuery($sql);
        $row =  $result->fetch_array(MYSQLI_ASSOC);
        return $row;
        $result->free();
    }

    public function all(): ?array
    {
        Db::getInstance()->link();
        $sql = $this->build();
        $result = \Packages\DB\Db::getInstance()->sendQuery($sql);
        if( $result = \Packages\DB\Db::getInstance()->sendQuery($sql)=== true) {
            echo"запрос успешно отправлен";
         } else {
            echo"запрос не отправлен";}
        $row =  $result->fetch_all(MYSQLI_ASSOC);
        return $row;
        $result->free();
    }
}

