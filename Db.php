<?php


namespace Packages\DB;

use mysqli;
use Packages\DB\ConfigurableInterface;

class Db implements ConfigurableInterface
{
    protected static $instance;
    protected $config = [];
    protected $mysqli;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function configure($config = [])
    {
        $this->config['host'] = $config['host'];
        $this->config['user'] = $config['user'];
        $this->config['pass'] = $config['pass'];
        $this->config['db'] = $config['db'];
    }

    public function link()
    {
        $this->mysqli = new mysqli(
            $this->config['host'],
            $this->config['user'],
            $this->config['pass'],
            $this->config['db']);
        if ($this->mysqli->connect_error) {
            die('Ошибка подключения (' . $this->mysqli->connect_errno . ') '
                . $this->mysqli->connect_error);
        }
        $this->mysqli->set_charset("utf8");
    }

    protected function __construct()
    {
    }

    protected function __clone()
    {
    }

    public function sendQuery($query)
    {
        return $this->mysqli->query($query);
    }

   /* public function __destruct()
    {
        if ($this->mysqli) $this->mysqli->close();
    }*/


}

