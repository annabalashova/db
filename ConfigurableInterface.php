<?php


namespace Packages\DB;


interface ConfigurableInterface
{
public function configure($config = []);
}